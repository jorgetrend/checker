<?php
include("BD.php");

$matricula_Docente = $_POST['matricula_docente'];
$contrasena_Docente = $_POST['contrasena_docente'];

$queryDocente = "Select * FROM docente WHERE matricula_doc ='$matricula_Docente'";

$respuestaDocente = mysqli_query($conexion_BD, $queryDocente); //Ejecutamos la consulta y guardamos su resultado

$docente = mysqli_fetch_array($respuestaDocente); // Agregamos en un array los resultados obtenidos
$id_docente = $docente['id_docente'];
$queryTema = "INSERT INTO tema VALUES(null, '$id_docente', 'bg-danger')";

date_default_timezone_set('America/Mexico_City');
$fecha_sesion = date("Y-m-d H:i:s");
$log = "Ingreso a cuenta";
$seguimiento = "INSERT INTO seguimiento_docente VALUES(null, '$id_docente', '$log', '$fecha_sesion')";

if ($matricula_Docente == $docente['matricula_doc'] ) { // Si existe una matricula registrada
  if (password_verify($contrasena_Docente, $docente['contrasena_doc']) ) {
      session_start();
      //Asignamos valores a nuestras variables de sesión.
      $_SESSION['matricula_docente'] =  $docente['matricula_doc'];
      $_SESSION['docente'] = $docente;
      $consultaTema = mysqli_query($conexion_BD, $queryTema);
      mysqli_query($conexion_BD, $seguimiento);
      header("location: InicioDocente.php");
  }else{
      echo '<script>
           alert("Contraseña incorrecta");
           window.history.go(-1);
           </script>';
  }//end else
} else {
  echo '<script>
       alert("Matricula no registrada");
       window.history.go(-1);
       </script>';
}//end else
?>
