<?php
include("BD.php");
include("includes/header.php");
include("BotonesActividad.php");
if (isset($_SESSION['matricula_docente'])) {

?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="InicioDocente.php">Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Actividades</li>
      </ol>
    </nav>

    <div class="container">
      <div class="textoPrincipal" style="text-align: center;">
        <h2>Actividades asignadas</h2>
        <hr>
        <br>
      </div>

      <div class="container">
        <center>
          <div class="row" style="margin-bottom: 30px;">
            <button style="margin-left: 50px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Actividad
            </button>

            <button style="margin-left: 20px" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal0" data-whatever="@mdo">Editar Actividad
            </button>

            <button style="margin-left: 20px" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">Eliminar Actividad
            </button>
          </div>

          <?php
          $id_docente = $_SESSION['docente']['id_docente'];
          $sql = "SELECT ac.id_actividad, ac.descripcion_act, ac.fecha_act,
                    u.nombre_ubi, p.descripcion_pri, e.nombre_est, es.descripcion_estado
                    FROM actividad ac, ubicacion u, prioridad_act p, estudiante e, estado_actividad es
                    WHERE ac.id_docente = '$id_docente'
                    AND ac.id_estudiante is not NULL
                    AND ac.id_ubicacion = u.id_ubicacion
                    AND ac.id_prioridad = p.id_prioridad
                    AND ac.id_estudiante = e.id_estudiante
                    AND ac.id_estado_act <> '3'
                    AND ac.id_estado_act = es.id_estado_act;";
          $resultadoActividades = mysqli_query($conexion_BD, $sql);
          /*Comprueba si existe algún estudiante registrado, y muestra un mensaje en caso negativo.*/
          $total = mysqli_num_rows($resultadoActividades);
          if ($total == 0) {
          ?>
            <div style="margin-left:auto; margin-right:auto;">
              <img style="height: 200px;" src="ImagenesUsuarios/Figuras/search.png" alt="vector Buscar">
              <h6 style="margin-bottom: 30px;">No hay actividades registradas :(</h6>
              <button style="margin-left: 50px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Actividad
              </button>
            </div>
          <?php
          } else { ?>
            <table class="table" id="actividades" style="margin-top: 15px;">
              <thead class="bg-info text-white">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Descripción</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Ubicación</th>
                  <th scope="col">Prioridad</th>
                  <th scope="col">Estudiante</th>
                  <th scope="col">Estado</th>
                </tr>
              </thead>
              <?php
              while ($tab = mysqli_fetch_array($resultadoActividades)) {    ?>
                <tbody>
                  <tr>
                    <th scope="row"><?php echo $tab['id_actividad'] ?></th>
                    <td><?php echo $tab['descripcion_act'] ?></td>
                    <td><?php echo $tab['fecha_act'] ?></td>
                    <td><?php echo $tab['nombre_ubi'] ?></td>
                    <td><?php echo $tab['descripcion_pri'] ?></td>
                    <td><?php echo $tab['nombre_est'] ?></td>
                    <td><?php echo $tab['descripcion_estado'] ?></td>
                  </tr>
                </tbody>
            <?php }
            } ?>
            </table>
        </center>
      </div>
      </center>
    </div>

    <!--Agregar una nueva actividad -->
    <div class="container mt-2 pt-2">

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-success text-white">
              <h5 class="modal-title" id="exampleModalLabel">Registrar Actividad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="AgregarActividad.php" method="post">

                <p>Ingresa los datos requeridos para registrar una nueva actividad.</p>
                <hr>
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Descripción de la actividad:</label>
                  <input type="text" class="form-control" id="recipient-name" name="descripcion" placeholder="Ej. Terminar el desarrollo." required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Ubicación de la actividad:</label>
                  <br>
                  <?php
                  $consulta = "SELECT * FROM ubicacion where id_docente = '$id_docente'";
                  $query = mysqli_query($conexion_BD, $consulta); ?>
                  <select name="ubicacion" required="true">
                    <?php while ($ubicaciones = mysqli_fetch_assoc($query)) { ?>
                      <option> <?php echo $ubicaciones['nombre_ubi'] ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Estudiante responsable:</label>
                  <br>

                  <?php

                  $consulta = "SELECT nombre_est as nombre FROM estudiante where id_docente = '$id_docente'";
                  $query = mysqli_query($conexion_BD, $consulta); ?>


                  <select name="estudiante_asignado">
                    <option>

                    </option>
                    <?php while ($asignarEstudiante = mysqli_fetch_assoc($query)) { ?>

                      <option>
                        <?php
                        echo $asignarEstudiante['nombre'];
                        ?>
                      </option>

                    <?php } ?>
                  </select>

                  <?php

                  ?>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Prioridad de la actividad: </label>
                  <br>
                  <select name="prioridad" required="true">
                    <option>Baja</option>
                    <option>Alta</option>
                    <option>Muy Alta</option>
                  </select>
                  <br>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Agregar una nueva activida-->

      <!-- Editar una actividad -->


      <div class="container mt-2 pt-2">

        <div class="modal fade" id="exampleModal0" tabindex="-3" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-warning text-white">
                <h5 class="modal-title" id="exampleModalLabel">Editar Actividad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                <p>Ingresa los datos requeridos para editar una actividad.</p>
                <hr>
                <form action="ActualizarActividad.php" method="post">
                  <div class="form-group">
                    <div class="modal-body">
                      <label for="recipient-name" class="col-form-label">N. de la actividad:</label>
                      <br>
                      <?php
                      $consulta = "SELECT * FROM actividad WHERE id_estado_act != '4' and id_docente = '$id_docente'";
                      $query = mysqli_query($conexion_BD, $consulta); ?>
                      <select name="id" required="true">
                        <?php while ($actividades = mysqli_fetch_assoc($query)) { ?>
                          <option> <?php echo $actividades['id_actividad'] ?></option>
                        <?php } ?>
                      </select>
                      <form action="ActualizarActividad.php" method="post">
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Nueva descripción de la actividad:</label>
                          <input type="txt" class="form-control" id="recipient-name" name="descripcion" placeholder="Ej. Actividad recreativa">
                        </div>

                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Ubicación de la actividad:</label>
                          <br>
                          <?php
                          $consulta = "SELECT * FROM ubicacion WHERE id_docente = '$id_docente'";
                          $query = mysqli_query($conexion_BD, $consulta); ?>
                          <select name="ubicacion" required="true">
                            <?php while ($ubicaciones = mysqli_fetch_assoc($query)) { ?>
                              <option> <?php echo $ubicaciones['nombre_ubi']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                  <label for="message-text" class="col-form-label">Prioridad de la actividad: </label>
                  <br>
                  <select name="prioridad" required="true">
                    <option>Baja</option>
                    <option>Alta</option>
                    <option>Muy Alta</option>
                  </select>
                  <br>
                </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                          <button type="submit" class="btn btn-warning">Actualizar Actividad</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Editar una actividad-->



      <!-- Eliminar una actividad-->


      <div class="container mt-2 pt-2">
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Actividad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <form action="EliminarActividad.php" method="post">
                    <p>Selecciona el número de actividad a eliminar.</p>
                    <label for="recipient-name" class="col-form-label">N. de Actividad:</label>

                    <?php
                    $consulta = "SELECT * FROM actividad WHERE id_estado_act != '4' and id_docente = '$id_docente'";
                    $query = mysqli_query($conexion_BD, $consulta); ?>
                    <select name="id" required="true">
                      <?php while ($actividades = mysqli_fetch_assoc($query)) { ?>
                        <option> <?php echo $actividades['id_actividad'] ?></option>
                      <?php } ?>
                    </select>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>





    </div>
    </div>

  </body>

  <!-- Fin Eliminar una actividad-->


  <?php include("includes/footerDocente.php"); ?>
  </body>


<?php } else {
  header("location: index.php");
} ?>