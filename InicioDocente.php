<?php
include("BD.php");
include("includes/header.php");

if (isset($_SESSION['matricula_docente'])) { //Si existe una variable de sesión
?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="InicioDocente.php">Inicio</a></li>
      </ol>
    </nav>

    <div class="container">
      <div class="textoPrincipal" style="text-align: center; margin-top:10px; margin-bottom: 20px;">
        <h2>Estudiantes Registrados</h2>
        <hr>
      </div>

      <div class="container mt-5 pt-10 ">
        <div class="row">
          <button style="margin-left: 50px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Estudiante
          </button>

          <button style="margin-left: 20px" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">Eliminar Estudiante
          </button>

          <a href="Permisos.php" class="btn btn-outline-primary " style="margin-left: 20px">Permisos</a>

        </div>

        <div class="row mt-5 pt-4" id="refrescar">
          <?php
          $id_docente = $_SESSION['docente']['id_docente'];
          $consultaEstudiante = "SELECT id_estudiante,
                        nombre_est,
                        matricula_est,
                        id_estado, licenciatura_est
                        FROM estudiante
                        where id_docente = '$id_docente'";

          $resultadoEstudiante = mysqli_query($conexion_BD, $consultaEstudiante);
          /*Comprueba si existe algún estudiante registrado, y muestra un mensaje en caso negativo.*/
          $total = mysqli_num_rows($resultadoEstudiante);
          if ($total == 0) {
          ?>
            <div style="margin-left:auto; margin-right:auto;">
              <img style="height: 150px;" src="ImagenesUsuarios/Figuras/searching.png" alt="vector Buscar">
              <h6 style="margin-bottom: 20px; margin-top:20px;">No hay estudiantes registrados :(</h6>
              <button style="margin-left: 30px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Estudiante
              </button>
            </div>
          <?php
          }
          while ($card = mysqli_fetch_array($resultadoEstudiante)) {
          ?>
            <div class="card border-dark mb-3" style=" margin-left: 30px; min-width:380px">

              <div class="card-header bg-info" style="color: white;">
                <h5><?php echo $card['nombre_est'] ?></h5>
              </div>
              <div class="card-body">
                <p class="card-text">Matricula: <?php echo $card['matricula_est'] ?></p>
                <p class="card-text">Licenciatura: <?php echo $card['licenciatura_est'] ?></p>
              </div>
              <div class="card-footer">
                <?php
                switch ($card['id_estado']) {
                  case 1:
                    $estado = "Disponible";
                ?>
                    <h6 class="text-success"><?php echo $estado; ?></h6>
                  <?php
                    break;
                  case 2:
                    $estado = "Ocupado";
                  ?>
                    <h6 class="text-danger"><?php echo $estado; ?></h6>
                  <?php
                    break;
                  case 3:
                    $estado = "Desconectado";
                  ?>
                    <h6 class=""><?php echo $estado; ?></h6>
                <?php
                    break;
                  default:
                    break;
                } ?>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>

      <!--Agregar Alumno-->

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header bg-success" style="color:white">
              <h5 class="modal-title" id="exampleModalLabel">Registrar Estudiante</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;
                </span>
              </button>
            </div>

            <div class="modal-body">
              <p>Ingresa los datos requeridos para registrar un nuevo estudiante.</p>
              <hr>
              <form action="registrarEstudiante.php" method="POST">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nombre del estudiante:</label>
                  <input type="text" class="form-control" id="recipient-name" name="nombre" placeholder="Ej. Estudiante Maravilla" required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Matricula del estudiante:</label>
                  <input type="text" class="form-control" id="message-text" name="matricula" placeholder="Ej. S17016281" required="true"></input>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Programa educativo:</label>
                  <input type="text" class="form-control" id="message-text" name="licenciatura" placeholder="Ej. Ingeniería de Software" required="true"></input>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Facultad de procedencia:</label>
                  <input type="text" class="form-control" id="message-text" name="facultad" placeholder="Ej. Contaduría y Administración"  required="true"></input>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Correo Electrónico:</label>
                  <input type="email" class="form-control" id="message-text" name="correo" placeholder="Ej. email@example.com" required="true"></input>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Contraseña del estudiante:</label>
                  <input type="password" class="form-control" name="contrasena" placeholder="Ej. contraseña123"  required="true" minlength="8"></input>
                </div>


                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <input type="submit" value="Registrar" class="btn btn-success" name="submit">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container mt-5 pt-10">


      <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header text-white bg-danger">
              <h5 class="modal-title" id="exampleModalLabel">Eliminar Estudiante</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="EliminarEstudiante.php" method="POST">

                <div class="form-group">
                  <p>Selecciona el estudiante que deseas eliminar.</p>
                  <label for="message-text" class="col-form-label">Estudiantes registrados:</label>
                  <?php
                  $idDocente = $_SESSION['docente']['id_docente'];
                  $consulta = "SELECT * FROM estudiante where id_docente = $idDocente";
                  $query = mysqli_query($conexion_BD, $consulta); ?>
                  <select name="estudiante" required="true">
                    <?php while ($alumno = mysqli_fetch_assoc($query)) { ?>
                      <option> <?php echo $alumno['nombre_est'] ?> </option>
                    <?php } ?>
                  </select>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <input type="submit" value="Eliminar" class="btn btn-danger" name="submit">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


  </body>


<?php include("includes/footerDocente.php");
} else { // Si no existe una variable de sesión, que rediriga al index para ingresar.
  header("location: index.php");
} ?>

<!--Fin Agregar Alumno-->