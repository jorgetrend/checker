<?php
include("BD.php");
include("includes/header.php");

if (isset($_SESSION['matricula_docente'])) {
?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="InicioDocente.php">Inicio</a></li>
        <li class="breadcrumb-item"><a href="InicioDocente.php">Estudiantes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Permisos</li>
      </ol>
    </nav>
    <div class="container">
      <div class="textoPrincipal" style="text-align: center; margin-top:10px;">
        <h2>Permisos</h2>
        <hr>
      </div>

      <div class="container"><?php //Aqui debo hacer el salto de linea
                              ?>
        <center>
          <?php
          $id_docente = $_SESSION['docente']['id_docente'];
          $sql = "SELECT * FROM permiso WHERE id_docente = '$id_docente'";
          $resultadoPermisos = mysqli_query($conexion_BD, $sql);
          /*Comprueba si existe algún permiso registrado, y muestra un mensaje en caso negativo.*/
          $total = mysqli_num_rows($resultadoPermisos);
          if ($total == 0) {
          ?>
            <div style="margin-left:auto; margin-right:auto;">
              <img style="height: 150px;" src="ImagenesUsuarios/Figuras/clip.png" alt="vector Buscar">
              <h6 style="margin-bottom: 20px; margin-top:20px;">No hay permisos registrados :(</h6>
            </div>
          <?php
          } else {

          ?>
            <table class="table">
              <thead class="bg-info text-white">
                <tr>
                  <th scope="col">Número</th>
                  <th scope="col">Estudiante</th>
                  <th scope="col">Descripción</th>
                  <th scope="col">Fecha</th>
                </tr>
              </thead>
              <?php
              while ($permiso = mysqli_fetch_array($resultadoPermisos)) {    ?>

                <tbody>
                  <tr>
                    <?php
                    $nombreEstudiante = $permiso['id_estudiante'];
                    $consultaTablaUbicacion = "SELECT nombre_est as nombre FROM estudiante where id_estudiante = '$nombreEstudiante'";
                    $queryTabla = mysqli_query($conexion_BD, $consultaTablaUbicacion);
                    $estudiante = mysqli_fetch_array($queryTabla); ?>

                    <th scope="row"><?php echo $permiso['id_permiso'] ?></th>
                    <td><?php echo $estudiante['nombre'] ?></td>
                    <td><?php echo $permiso['descripcion_per'] ?></td>
                    <td><?php echo $permiso['fecha_per'] ?></td>
                  </tr>
                </tbody>
            <?php }
            } ?>
            </table>
      </div>
      </center>
    </div>



    <center>
      <div class="btn-group" role="group" aria-label="Basic example">
        <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Permiso
        </button>

        <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModal0" data-whatever="@mdo">Editar Permiso
        </button>

        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">Eliminar Permiso
        </button>

      </div>
    </center>



    <div class="container mt-2 pt-2">

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-header bg-success text-white">
              <h5 class="modal-title" id="exampleModalLabel">Registrar Permiso</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Ingresa los datos requeridos para generar un nuevo permiso.</p>
              <hr>
              <form action="AgregarPermiso.php" method="post">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Descripción del permiso:</label>
                  <input type="text" class="form-control" id="recipient-name" name="descripcion_permiso" placeholder="Ej. Permiso por vacaciones." required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Estudiante:</label>
                  <?php
                  $consulta = "SELECT nombre_est as nombre FROM estudiante WHERE id_docente = '$id_docente'";
                  $query = mysqli_query($conexion_BD, $consulta); ?>
                  <select name="estudiante_asignado" required="true">
                    <?php while ($asignarEstudiante = mysqli_fetch_array($query)) { ?>
                      <option> <?php echo $asignarEstudiante['nombre'] ?> </option>

                    <?php } ?>
                  </select>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Agregar un nuevo permiso-->

      <!-- Editar permiso -->
      <div class="container mt-2 pt-2">
        <div class="modal fade" id="exampleModal0" tabindex="-3" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-warning text-white">
                <h5 class="modal-title" id="exampleModalLabel">Editar Permiso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                <p>Ingresa los datos para editar el permiso.</p>
                <hr>
                <form action="ActualizarPermiso.php" method="post">
                  <div class="form-group">
                    <div class="modal-body">
                      <label for="recipient-name" class="col-form-label">Número del permiso:</label>
                      <br>
                      <?php
                      $consulta = "SELECT * FROM permiso WHERE id_docente = '$id_docente'";
                      $query = mysqli_query($conexion_BD, $consulta); ?>
                      <select name="id" required="true">
                        <?php while ($permiso = mysqli_fetch_assoc($query)) { ?>
                          <option> <?php echo $permiso['id_permiso'] ?></option>
                        <?php } ?>
                      </select>
                      <form action="ActualizarPermiso.php" method="post">
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Nueva descripción del permiso:</label>
                          <input type="text" class="form-control" id="recipient-name" name="descripcion" placeholder="Ej. Actividad recreativa" required="true">
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                          <button type="submit" class="btn btn-warning">Actualizar Permiso</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Editar una actividad-->
    </div>


    <!-- Eliminar un Permiso-->
    <div class="container mt-2 pt-2">
      <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-danger text-white">
              <h5 class="modal-title" id="exampleModalLabel">Eliminar Permiso</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <form action="EliminarPermiso.php" method="post">
                  <p>Selecciona el número de permiso a eliminar.</p>
                  <label for="recipient-name" class="col-form-label">N. de Permiso:</label>

                  <?php
                  $consulta = "SELECT * FROM permiso WHERE id_docente = '$id_docente'";
                  $query = mysqli_query($conexion_BD, $consulta); ?>
                  <select name="id" required="true">
                    <?php while ($permiso = mysqli_fetch_assoc($query)) { ?>
                      <option> <?php echo $permiso['id_permiso'] ?></option>
                    <?php } ?>
                  </select>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
  </body>

  <!-- Fin Eliminar un Permiso-->
<?php
  include("includes/footerDocente.php");
} else {
  header("location: index.php");
} ?>