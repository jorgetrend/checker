<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Acerca De Nosotros</title>
  <!--BOOTSTRAP-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
  <!-- Banner Area -->
  <nav class="navbar navbar-dark bg-success">

    <h3>Checker <span class="badge badge-secondary">Control</span></h3>
    <div class="container">
      <a href="InicioDocente.php" class="navbar-brand"> Inicio</a>
      <a href="loginDocente.php" class="navbar-brand"> Docente</a>
      <a href="loginEstudiante.php" class="navbar-brand"> Estudiante</a>
      <a href="informacion.php" class="navbar-brand" style="color: yellowgreen"> Acerca de nosotros</a>
    </div>
  </nav>


  <div class="banner-area">
    <img src="img/coatza.jpg" style="width:100%; height:520px;" alt=" Ciudad de Coatzacoalcos">
    <h1 class="bg-success" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white;">Acerca de Nosotros</h1>
  </div>
  <!-- Banner Area -->
  <div class="site-section mb-5">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-12 mb-5 text-center mt-5" style="margin-top: 2%">
          <h2>Nuestra historia</h2>
        </div>
        <div class="col-md-6">
          <p class="lead" style="text-align: center">¡Si puedes soñarlo, nosotros lo creamos!.</p>
          <p style="text-align: justify">Watermelon Co. surge en la ciudad y puerto de Coatzacoalcos, Veracruz a raíz de la necesidad de nuevas empresas encargadas de proveer soluciones tecnológicas en el mundo laboral actual.
            <br> Constituído en el año 2020, Watermelon Co. rápidamente se posicionó entre los principales referentes del desarrollo de soluciones tecnológicas.
            <br> Bajo el lema de: <strong><em>"La comodidad alimenta la debilidad"</em></strong> nos enfocamos en ofrecer las mejores soluciones tecnológicas a las necesidades de tu empresa.</p>
        </div>
        <div class="col-md-6">
          <img src="img/logoWatermelon.jpg" alt="Logo Watermelon Co.">
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-md-12 mb-5 text-center mt-5">
          <h2>Nuestro Equipo</h2>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%;margin-top: 3%;">
          <img src="img/Marcelo.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Jorge Marcelo</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Scrum Master.</h6>
            <p style="text-align: justify"><em>"Estudiante de Ingeniería de Software en la Universidad Veracruzana. Me apasiona el desarrollo de software y documentación que todo la calidad conlleva. Mi rol en este proyecto fue de Scrum Master, el cual requiere muchas responsabilidades y habilidades blandas. Somos un excelente equipo dentro de WatermelonCo."</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%; margin-top: 3%;">
          <img src="img/Fernandez.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">José Luis</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Product Owner.</h6>
            <p style="text-align: justify"><em>"Desarrollamos la consultoría y análisis para diseñar la mejor solución alineada con el cumplimiento de tus objetivos dentro de tu estrategia."</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%; margin-top: 3%;">
          <img src="img/Dominguez.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Jorge Daniel</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Diseño.</h6>
            <p style="text-align: justify"><em>"Soy un amante del arte"</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%;margin-top: 3%;">
          <img src="img/Chiz.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Bryan Javier</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Diseño.</h6>
            <p style="text-align: justify"><em>"Un gran desarrollo conlleva una gran responsabilidad"</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%; margin-top: 3%;">
          <img src="img/Yael.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Rodrigo Yael</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Desarrollo.</h6>
            <p style="text-align: justify"><em>"if ( puedes pensarlo ) <br>{ echo 'puede hacerse';}"</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%; margin-top: 3%;">
          <img src="img/Zurita.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Rene Gabriel</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Desarrollo.</h6>
            <p style="text-align: justify"><em>"No es sobre las ideas. Sino hacer que éstas se vuelvan raelidad"<br> - Scott Belsky</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%;margin-top: 3%;">
          <img src="img/Jafeth.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Jafet Antonio</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Desarrollo.</h6>
            <p style="text-align: justify"><em>"Si algo malo puede pasar, pasará"<br> - Ley de Murphy</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%; margin-top: 3%;">
          <img src="img/Marlon.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Marlon Francisco</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Desarrollo.</h6>
            <p style="text-align: justify"><em>"Dormir, ¿Qué es eso?"</em></p>
          </div>
        </div>

        <!---->
        <div class="card border-success mb-3" style="width: 18rem; text-align:center; margin-left:3%; margin-right: 3%; margin-top: 3%;">
          <img src="img/Valerio.jpg" width="100" height="300" class="card-img-top" alt="Integrante del equipo.">
          <div class="card-header bg-success">
            <h4 class="card-title" style="color: white">Carlos Daniel</h4>
          </div>
          <div class="card-body">
            <h6 class="card-text "> Equipo de Documentación.</h6>
            <p style="text-align: justify"><em>"...Brindar por el amor, sufrir por el dolor, tomando una cerveza, la vida se ve mucho mejor..."<br> - Panteón Rococó</em></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  include("includes/footerDocente.php");
  ?>
</body>

</html>
