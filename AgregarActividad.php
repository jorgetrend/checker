<?php
include("BD.php");

session_start();
$descripcion_actividad    = $_POST['descripcion'];

$nombre_ubicacion  = $_POST['ubicacion'];
$consultaUbicacion = "SELECT id_ubicacion from ubicacion where nombre_ubi = '$nombre_ubicacion'";
$queryUbicacion = mysqli_query($conexion_BD, $consultaUbicacion);
$arrayUbicacion = mysqli_fetch_array($queryUbicacion);
$id_ubicacion = $arrayUbicacion['id_ubicacion'];
$id_docente = $_SESSION['docente']['id_docente'];
date_default_timezone_set('America/Mexico_City');
$fecha_actividad = date("Y-m-d H:i:s");
$estudianteAsignado = $_POST['estudiante_asignado'];

$prioridad    = $_POST['prioridad'];
switch ($prioridad) {
    case "Baja":
        $prioridad = 1;
        break;
    case "Alta":
        $prioridad = 2;
        break;
    case "Muy Alta":
        $prioridad = 3;
        break;
    default:
        break;
}

if (empty($estudianteAsignado)) {
  $sql = "INSERT INTO actividad VALUES (null, '$id_docente', null, '$id_ubicacion', '$prioridad', 0,
                                        '$descripcion_actividad', '$fecha_actividad', '','')";
}else {
  $consultaID = "SELECT id_estudiante from estudiante where nombre_est = '$estudianteAsignado'";
  $query = mysqli_query($conexion_BD, $consultaID);
  $array = mysqli_fetch_array($query);
  $id = $array['id_estudiante'];
  $sql = "INSERT INTO actividad VALUES (null, '$id_docente', '$id', '$id_ubicacion', '$prioridad', 0,
                                        '$descripcion_actividad', '$fecha_actividad', '','')";
}

$ejecutar = mysqli_query($conexion_BD, $sql);
if (!$ejecutar) {
    echo "Error al registrar la actividad\n";

} else {
    echo '<script>
          alert("Actividad registrada correctamente");
          </script>';


          $log = "Creacion de actividad"." ".$descripcion_actividad;
          $seguimiento = "INSERT INTO seguimiento_docente VALUES(null, '$id_docente', '$log', '$fecha_actividad')";
          mysqli_query($conexion_BD, $seguimiento);
          
    header("location: InicioActividades.php");
    die();
}
