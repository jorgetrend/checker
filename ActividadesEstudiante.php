<?php
include("BD.php");
include("includes/headerEstudiante.php");
if (isset($_SESSION['matricula_estudiante'])) {
?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="ActividadesEstudiante.php">Inicio</a></li>
      </ol>
    </nav>
    <div class="container">
      <center>
        <h2>Actividades asignadas</h2>
        <hr>
        <br>
      </center>
      <div class="container"><?php //Aqui debo hacer el salto de linea
                              ?>
        <center>
          <?php
          $id_estudiante = $_SESSION['Estudiante']['id_estudiante'];
          $sql = "SELECT ac.id_actividad, ac.descripcion_act, ac.fecha_act, u.nombre_ubi, e.descripcion_estado, ac.Observacion_Doc
          FROM actividad ac, ubicacion u, estado_actividad e
          WHERE ac.id_estudiante = '$id_estudiante'
          AND ac.id_ubicacion = u.id_ubicacion
          AND ac.id_estado_act = e.id_estado_act";
          $resultadoActividades = mysqli_query($conexion_BD, $sql);
          $total = mysqli_num_rows($resultadoActividades);
          if ($total == 0) {
          ?>
            <div style="margin-left:auto; margin-right:auto;">
              <img style="height: 150px;" src="ImagenesUsuarios/Figuras/searching.png" alt="vector Buscar">
              <h6 style="margin-bottom: 20px; margin-top:20px;">No hay actividades asignadas :D</h6>
              
            </div>
          <?php
          } else {
          ?>
            <table class="table">
              <thead class="bg-info text-white">
                <tr>
                  <th scope="col">Número</th>
                  <th scope="col">Descripción</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Ubicación</th>
                  <th scope="col">Estado</th>
                  <th scope="col">Observaciones</th>
                </tr>
              </thead>
              <?php
              while ($tab = mysqli_fetch_array($resultadoActividades)) {    ?>
                <tbody>
                  <tr>
                    <th scope="row"><?php echo $tab['id_actividad'] ?></th>
                    <td><?php echo $tab['descripcion_act'] ?></td>
                    <td><?php echo $tab['fecha_act'] ?></td>
                    <td><?php echo $tab['nombre_ubi'] ?></td>
                    <td><?php echo $tab['descripcion_estado'] ?></td>
                    <td><?php echo $tab['Observacion_Doc'] ?></td>
                  </tr>
                </tbody>
              <?php } ?>
            </table>
          <?php } ?>

        </center>
      </div>

      <center>
        <h2>Actividades sin asignar</h2>
        <hr>
        <br>
      </center>
      <div class="container"><br>
        <center>
          <table class="table">
            <thead class="bg-info text-white">
              <tr>
                <th scope="col">Número</th>
                <th scope="col">Descripción</th>
                <th scope="col">Fecha</th>
                <th scope="col">Ubicación</th>
              </tr>
            </thead>
            <?php

            $consulta = "SELECT id_docente from estudiante where id_estudiante ='$id_estudiante'";
            $resultadoEst = mysqli_query($conexion_BD, $consulta);
            $array = mysqli_fetch_array($resultadoEst);
            $id_docente = $array['id_docente'];
            $sql = "SELECT ac.id_actividad, ac.descripcion_act, ac.fecha_act, ac.id_docente, u.nombre_ubi
            FROM actividad ac, ubicacion u
            WHERE ac.id_estudiante is null
            AND ac.id_ubicacion = u.id_ubicacion
            AND ac.id_docente = '$id_docente'";
            $resultadoActividades = mysqli_query($conexion_BD, $sql);

            while ($tab = mysqli_fetch_array($resultadoActividades)) {    ?>
              <tbody>
                <tr>
                  <th scope="row"><?php echo $tab['id_actividad'] ?></th>
                  <td><?php echo $tab['descripcion_act'] ?></td>
                  <td><?php echo $tab['fecha_act'] ?></td>
                  <td><?php echo $tab['nombre_ubi'] ?></td>

                </tr>
              </tbody>
            <?php } ?>
          </table>
        </center>
      </div>

    </div>

    <div class="container mt-5 pt-5">
      <button style="margin-left: 150px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Actividad
      </button>
      <button style="margin: 5px" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModa2" data-whatever="@mdo">Tomar Actividad
      </button>

      <button type="button" style="margin-left: 5px" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModa4" data-whatever="@mdo">Empezar actividad
      </button>

      <button type="button" style="margin: 5px" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModa5" data-whatever="@mdo">Pausar actividad
      </button>

      <button style="margin-left: 15px" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModa3" data-whatever="@mdo">Concluir actividad
      </button>


      <!-- Modals -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-success text-white">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Actividad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Ingresa los datos requeridos para registrar una actividad.</p>
              <hr>
              <form action="AgregarActividadEstudiante.php" method="POST">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Descripción:</label>
                  <input type="text" class="form-control" id="recipient-name" name="descripcion" placeholder="Ej. Actividad N.1" required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Ubicación:</label>
                  <br>
                  <?php
                  $consulta = "SELECT * FROM ubicacion where  id_docente = '$id_docente'";
                  $query = mysqli_query($conexion_BD, $consulta); ?>
                  <select name="ubicacion" required="true">
                    <?php while ($ubicaciones = mysqli_fetch_assoc($query)) { ?>

                      <option> <?php echo $ubicaciones['nombre_ubi'] ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Prioridad:</label>
                  <br>
                  <select name="prioridad" required="true">
                    <option>Baja</option>
                    <option>Alta</option>
                    <option>Muy Alta</option>
                  </select>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="exampleModa2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-info text-white">
              <h5 class="modal-title" id="exampleModalLabel">Tomar Actividad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="tomarActividadEstudiante.php" method="POST">

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Selecciona la actividad:</label>
                  <br>
                  <?php
                  $consulta1 = "SELECT descripcion_act FROM actividad
                              WHERE id_estudiante is null
                              AND id_docente = '$id_docente' ";
                  $query1 = mysqli_query($conexion_BD, $consulta1); ?>
                  <select name="descripcion" required="true">
                    <?php while ($act = mysqli_fetch_assoc($query1)) { ?>
                      <option><?php echo $act['descripcion_act']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-info text-black">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <!--  -->
      <div class="modal fade" id="exampleModa3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-danger text-white">
              <h5 class="modal-title" id="exampleModalLabel">Concluir actividad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="concluirActividad.php" method="POST">

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Selecciona la actividad:</label>
                  <br>
                  <?php
                  $consulta1 = "SELECT descripcion_act FROM actividad
                              WHERE id_estado_act = '1'
                              AND id_estudiante = '$id_estudiante'";
                  $query1 = mysqli_query($conexion_BD, $consulta1); ?>
                  <select name="actividad" required="true">
                    <?php while ($act = mysqli_fetch_assoc($query1)) { ?>
                      <option><?php echo $act['descripcion_act']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Observaciones</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="Observaciones" required="true" placeholder="Ej. Actividad concluida sin éxito."></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-danger">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="exampleModa4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-success text-white">
              <h5 class="modal-title" id="exampleModalLabel">Empezar actividad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="empezarActividad.php" method="POST">

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Selecciona la actividad:</label>
                  <br>
                  <?php
                  $consulta1 = "SELECT descripcion_act FROM actividad
                              WHERE id_estudiante = '$id_estudiante' AND id_estado_act = '0' or id_estado_act = '2'";
                  $query1 = mysqli_query($conexion_BD, $consulta1); ?>
                  <select name="actividad" required="true">
                    <?php while ($act = mysqli_fetch_assoc($query1)) { ?>
                      <option><?php echo $act['descripcion_act']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="exampleModa5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-warning ">
              <h5 class="modal-title" id="exampleModalLabel">Pausar Actividad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="pausarActividad.php" method="POST">

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Selecciona la actividad:</label>
                  <br>
                  <?php
                  $consulta1 = "SELECT descripcion_act FROM actividad
                              WHERE id_estudiante = '$id_estudiante' AND id_estado_act = '1'";
                  $query1 = mysqli_query($conexion_BD, $consulta1); ?>
                  <select name="actividad" required="true">
                    <?php while ($act = mysqli_fetch_assoc($query1)) { ?>
                      <option><?php echo $act['descripcion_act']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-warning">Guardar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

  </body>

<?php include("includes/footer.php");
} else {
  header("location: index.php");
} ?>
