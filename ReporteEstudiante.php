<?php
//include pdf_mc_table.php, not fpdf17/fpdf.php
include('pdf_mc_table.php');
include('BD.php');
session_start();

//make new object
$pdf = new PDF_MC_Table();

//add page, set font
$pdf->AddPage();

$pdf->SetFont('Arial','B',15);
    // Movernos a la derecha
    $pdf->Cell(80);
    // Título
	$pdf->Cell(30,10,utf8_decode('Reporte de Actividades Realizadas'),0,0,'C');
	//Dibujo de la línea
	$pdf->Line(30,20,175,20);

    // Salto de línea
    $pdf->Ln(20);

//set width for each column (6 columns)
$pdf->SetWidths(Array(10,40,50,30,25,30));

//set alignment
$pdf->SetAligns(Array('C','','','C','C','C'));

//set line height. This is the height of each lines, not rows.
$pdf->SetLineHeight(10);

//add table heading using standard cells
//set font to bold
$pdf->SetFont('Arial','B',14);
$pdf->Cell(10,8,"#",1,0);
$pdf->Cell(40,8,"Estado",1,0);
$pdf->Cell(50,8,utf8_decode('Descripción'),1,0);
$pdf->Cell(30,8,"Fecha",1,0);
$pdf->Cell(25,8,"Prioridad",1,0);
$pdf->Cell(30,8,utf8_decode('Ubicación'),1,0);

$pdf->Ln();

//reset font
$pdf->SetFont('Arial','',14);

//Obtiene los valores de la BD.
$id_est = $_SESSION['Estudiante']['id_estudiante'];
$sql = "SELECT * FROM actividad where id_estudiante = '$id_est'";
$res = mysqli_query($conexion_BD, $sql);

while ($arrayActividad = mysqli_fetch_array($res))
{
  $id_ubicacion = $arrayActividad['id_ubicacion'];
  $id_prioridad = $arrayActividad['id_prioridad'];
  $id_estado_act = $arrayActividad['id_estado_act'];
  $consultaEstado = "SELECT * FROM estado_actividad where id_estado_act = '$id_estado_act'";
  $resultadoEstado = mysqli_query($conexion_BD, $consultaEstado);
  $estado = mysqli_fetch_array($resultadoEstado);


  $consultaUbicacion = "SELECT * FROM ubicacion where id_ubicacion = '$id_ubicacion'";
  $resultadoUbi = mysqli_query($conexion_BD, $consultaUbicacion);
  $ubicacion = mysqli_fetch_array($resultadoUbi);
  $consultaPrioridad = "SELECT * FROM prioridad_act where id_prioridad = '$id_prioridad'";
  $resultadoPri = mysqli_query($conexion_BD, $consultaPrioridad);
  $prioridad = mysqli_fetch_array($resultadoPri);
  $id_estudiante = $arrayActividad['id_estudiante'];
  $consultaEstudiante = "SELECT * FROM estudiante where id_estudiante = '$id_estudiante'";
  $resultadoEst = mysqli_query($conexion_BD, $consultaEstudiante);
  $estudiante = mysqli_fetch_array($resultadoEst);

  $pdf->Row(Array(
	$arrayActividad['id_actividad'],
	utf8_decode($estado['descripcion_estado']),
	utf8_decode($arrayActividad['descripcion_act']),
	$arrayActividad['fecha_act'],
	$prioridad['descripcion_pri'],
	utf8_decode($ubicacion['nombre_ubi']),
));

}


//output the pdf
$pdf->Output();
