<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>

    <meta charset="utf-8">
    <title>Inicio </title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  </head>



    <?php
    //--- AQUÍ VA LA CONSULTA PARA OBTENER EL VALOR
    session_start();
    $estudiante = $_SESSION['Estudiante']['id_estudiante'];
    $sql = "SELECT color_header FROM tema WHERE id_estudiante = '$estudiante' ";
    $queryTabla = mysqli_query($conexion_BD, $sql);
    $colores = mysqli_fetch_array($queryTabla);
    $variable = $colores['color_header'];
    //--- Aquí termina la consulta
    ?>
    <nav class="navbar navbar-dark <?php echo $variable ?>">
          <a title="" href="ActividadesEstudiante.php" style="margin-left: -50px">
              <img src="img/logo.png" alt="Logo Checker" style="width: 40px;
                  height: 40px;" />
              <span class="badge badge-dark">Checker</span>
          </a>
          <a href="ActividadesEstudiante.php" class="navbar-brand" > Actividades </a>
          <a href="ReporteEstudiante.php" target="_blank" class="navbar-brand" > Reporte </a>
          <div class="dropdown">
            <button class="btn btn-succes dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $nombre = $_SESSION['Estudiante']['nombre_est'];
              echo $nombre; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="temaEstudiante.php">Temas</a>
              <a class="dropdown-item" href="PerfilAlumno.php">Perfil</a>
              <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
            </div>
          </div>

    </nav>
