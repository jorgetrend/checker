<!DOCTYPE html>
<html lang="es">

<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
  <?php
  include('BD.php');
  //--- AQUÍ VA LA CONSULTA PARA OBTENER EL VALOR
  if (isset($_SESSION['matricula_estudiante'])) {
    $estudiante = $_SESSION['Estudiante']['id_estudiante'];
    $sql = "SELECT color_header FROM tema WHERE id_estudiante = '$estudiante' ";
    $queryTabla = mysqli_query($conexion_BD, $sql);
    $colores = mysqli_fetch_array($queryTabla);
    $variable = $colores['color_header'];
  } else {
    $variable = "bg-success";
  }

  //--- Aquí termina la consulta
  ?>
  <footer>
    <div class="foot <?php echo $variable; ?>" style="padding: 1.5%; text-align:center; color: white; margin-top: 5%;">
      <a> Watermelon Co. &copy; Todos los derechos reservados.</a>
    </div>
  </footer>
</body>

</html>
<!--scripts-->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>