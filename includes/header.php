<?php include("BD.php");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>CheckerApp </title>
  <!--BOOTSTRAP-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!--<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>-->
</head>

  <?php
  //--- AQUÍ VA LA CONSULTA PARA OBTENER EL VALOR
  session_start();
  $docente = $_SESSION['docente']['id_docente'];
  $sql = "SELECT color_header FROM tema WHERE id_docente = '$docente' ";
  $queryTabla = mysqli_query($conexion_BD, $sql);
  $colores = mysqli_fetch_array($queryTabla);
  $variable = $colores['color_header'];
  //--- Aquí termina la consulta
  ?>
  <nav class="navbar navbar-dark <?php echo $variable ?>">
      <a title="Inicio" href="InicioDocente.php">
        <img src="img/logo.png" alt="Logo Checker" style="width: 40px;
            height: 40px;" />
        <span class="badge badge-dark">Checker</span>
      </a>
      <a href="InicioDocente.php" class="navbar-brand" style="hover"> Estudiantes</a>
      <a href="InicioActividades.php" class="navbar-brand"> Actividades</a>
      <a href="InicioReportes.php" class="navbar-brand"> Reportes</a>
      <a href="InicioUbicacion.php" class="navbar-brand"> Ubicaciones</a>
      <div class="dropdown">
        <button style="color:white;" class="btn btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php
          $nombre = $_SESSION['docente']['nombre_doc'];
          echo $nombre; ?>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

          <a class="dropdown-item" href="temas.php">Temas</a>
          <a class="dropdown-item" href="PerfilDocente.php">Mi Perfil</a>
          <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
        </div>
      </div>

  </nav>