<?php
include("BD.php");
session_start();

if (isset($_SESSION['matricula_estudiante'])) {
  $matricula_estudiante = $_SESSION['matricula_estudiante'];
  $consultaEstado = "UPDATE estudiante set id_estado = 3 where matricula_est = '$matricula_estudiante'";
  mysqli_query($conexion_BD, $consultaEstado);
  date_default_timezone_set('America/Mexico_City');
  $fecha_sesion = date("Y-m-d H:i:s");
  $id_estudiante = $_SESSION['Estudiante']['id_estudiante'];

  $consultaTiempo = "SELECT MAX(id_tiempo_servicio) as id_mayor
                    FROM `tiempo_servicio`
                    WHERE id_estudiante = '$id_estudiante'";
  $resultadoTiempo = mysqli_query($conexion_BD, $consultaTiempo);
  $tiempoArray = mysqli_fetch_array($resultadoTiempo);

  $tiempo = $tiempoArray['tiempo'];
  $maximo = $tiempoArray['id_mayor'];
  $sql0 = "UPDATE tiempo_servicio
           SET hora_fin = '$fecha_sesion'
           where id_estudiante = '$id_estudiante'
           and id_tiempo_servicio = '$maximo' ";
  mysqli_query($conexion_BD, $sql0);
  $consultaDiferencia = " SELECT TIME_TO_SEC(TIMEDIFF( MAX(hora_fin), MAX(hora_inicio))) as tiempo
                          from tiempo_servicio
                          where id_estudiante = '$id_estudiante'
                          and id_tiempo_servicio = '$maximo'";
  $tiempoDiferencia = mysqli_query($conexion_BD, $consultaDiferencia);
  $tiempoDif = mysqli_fetch_array($tiempoDiferencia);
  $totalHoras = $tiempoDif['tiempo'];

  $totHoras ="UPDATE tiempo_servicio
           SET total_horas = '$totalHoras'
           where id_estudiante = '$id_estudiante'
           and id_tiempo_servicio = '$maximo'";
  mysqli_query($conexion_BD, $totHoras);
}elseif (isset($_SESSION['matricula_docente'])) {
  $id_docente = $_SESSION['docente']['id_docente'];
  date_default_timezone_set('America/Mexico_City');
  $fecha_sesion = date("Y-m-d H:i:s");
  $log = "Cierre de cuenta";
  $seguimiento = "INSERT INTO seguimiento_docente VALUES(null, '$id_docente', '$log', '$fecha_sesion')";
  mysqli_query($conexion_BD, $seguimiento);
}




session_destroy();
header("location: index.php");
exit();


 ?>
