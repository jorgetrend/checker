<?php include("BD.php");
include("includes/header.php");

if (isset($_SESSION['matricula_docente'])) {
?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="InicioDocente.php">Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Reportes</li>
      </ol>
    </nav>

    <div class="container">
      <div class="textoPrincipal" style="text-align: center; margin-top:10px;">
        <h2>Reportes</h2>
        <hr>

        <a target="_blank" href="ReporteGeneral.php">
          <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Generar Reporte General
          </button>
        </a>

      </div>
    </div>

    <div class="container mt-5 pt-10 ">

      <div class="row mt-5 pt-4">
        <?php
        $id_docente = $_SESSION['docente']['id_docente'];
        $consultaEstudiante = "SELECT id_estudiante,
                        nombre_est,
                        matricula_est,
                        id_estado, licenciatura_est
                        FROM estudiante
                        where id_docente = '$id_docente'";

        $resultadoEstudiante = mysqli_query($conexion_BD, $consultaEstudiante);
        /*Comprueba si existe algún estudiante registrado, y muestra un mensaje en caso negativo.*/
        $total = mysqli_num_rows($resultadoEstudiante);
        if ($total == 0) {
        ?>
          <div style="margin-left:auto; margin-right:auto;">
            <img style="height: 150px;" src="ImagenesUsuarios/Figuras/searching.png" alt="vector Buscar">
            <h6 style="margin-bottom: 20px; margin-top:20px;">No hay estudiantes registrados :(</h6>
            <a href="InicioDocente.php"><button style="margin-left: 30px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Agregar Estudiante
            </button></a>
          </div>
        <?php
        }
        while ($card = mysqli_fetch_array($resultadoEstudiante)) {                ?>
          <div class="card border-dark mb-3" style=" margin-left: 30px; min-width:350px">

            <div class="card-header bg-info" style="color: white;">
              <h5><?php echo $card['nombre_est'] ?></h5>
            </div>
            <div class="card-body">
              <p class="card-text">Matricula: <?php echo $card['matricula_est'] ?></p>
              <p class="card-text">Licenciatura: <?php echo $card['licenciatura_est'] ?></p>
            </div>
            <div class="card-footer">
              <?php $id =  $card['id_estudiante']; ?>
              <h6><a target="_blank" href="Reporte.php?id=<?php echo $id ?>">Generar Reporte Individual</a></h6>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>


    <?php
    include("includes/footerDocente.php");
    ?>
  </body>

<?php } else {
  header("location: index.php");
} ?>