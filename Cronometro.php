<?php
include("BD.php");
include("includes/headerEstudiante.php");


date_default_timezone_set('America/Mexico_City');


if (isset($_SESSION['matricula_estudiante'])) {

  $id_estudiante = $_SESSION['Estudiante']['id_estudiante'];
  $booleanIniciar = TRUE;
?>

  <form method="post">
    <div class="row">
      <input style="margin:0 auto" class="btn btn-success" text-align="center" type="submit" name="iniciarCronometro" id="iniciar" value="Iniciar Cronómetro" /><br />

      <input style="margin:0 auto" style="margin-top: 100px" class="btn btn-danger" type="submit" name="terminarCronometro" id="terminar" value="Terminar Cronómetro" />

      <input style="margin:0 auto" style="margin-top: 100px" class="btn btn-warning" type="submit" name="verTiempo" id="tiempo" value="Ver tiempo " />
    </div>
  </form>

  <?php


  function iniciarCronometro($conexion, $id_estudiante, $boolean)
  {
    if ($boolean) {

      $horaInicio = date("Y-m-d H:i:s");
      $insertar = "INSERT INTO tiempo_servicio values ('$id_estudiante', '$horaInicio', null, null)";
      $ejecutar = mysqli_query($conexion, $insertar);
      if ($ejecutar) { ?>

        <script>
          alert('Has inciado el cronómetro!');
        </script>

  <?php  }
      $booleanIniciar = FALSE;
    } else {
      echo "ya iniciaste el cronometro hdp";
    }
  }
  ?>

  <?php
  function terminarCronometro($conexion, $horaFin, $id_estudiante)
  {
    $terminar = "UPDATE tiempo_servicio set hora_fin = '$horaFin' where id_estudiante = '$id_estudiante'";
    $ejecutar = mysqli_query($conexion, $terminar);
    if ($ejecutar) { ?>

      <script>
        alert('Has terminado el cronómetro!');
      </script>
  <?php   }
  }
  ?>

  <?php
  function verTiempo($conexion, $id_estudiante)
  {
    $consulta = "SELECT TIMEDIFF(hora_inicio, hora_fin) as tiempo from tiempo_servicio where id_estudiante='$id_estudiante'";
    $ejecutar = mysqli_query($conexion, $consulta);
    $array = mysqli_fetch_array($ejecutar);
    echo "Usted ha cubierto";
    echo $array['tiempo'];
    echo "Horas";
  }

  if (isset($_POST['iniciarCronometro'])) {
    iniciarCronometro($conexion_BD, $id_estudiante, $booleanIniciar);
  } elseif (isset($_POST['terminarCronometro'])) {

    $horaFin = date("Y-m-d H:i:s");
    terminarCronometro($conexion_BD, $horaFin, $id_estudiante);
    $booleanTerminar = TRUE;
  } elseif (isset($_POST['verTiempo'])) {

    verTiempo($conexion_BD, $id_estudiante);
  }



  ?>


<?php
} else {
} ////////-----------
?>


<script>
  var inicio = 0;
  var timeout = 0;

  function empezarDetener(elemento) {
    if (timeout == 0) {
      // empezar el cronometro

      elemento.value = "";

      // Obtenemos el valor actual
      inicio = new Date().getTime();

      // Guardamos el valor inicial en la base de datos del navegador
      localStorage.setItem("inicio", inicio);

      // iniciamos el proceso
      funcionando();
    } else {
      // detemer el cronometro

      elemento.value = "";
      clearTimeout(timeout);

      // Eliminamos el valor inicial guardado
      localStorage.removeItem("inicio");
      timeout = 0;
    }
  }

  function funcionando() {
    // obteneos la fecha actual
    var actual = new Date().getTime();

    // obtenemos la diferencia entre la fecha actual y la de inicio
    var diff = new Date(actual - inicio);

    // mostramos la diferencia entre la fecha actual y la inicial
    var result = LeadingZero(diff.getUTCHours()) + ":" + LeadingZero(diff.getUTCMinutes()) + ":" + LeadingZero(diff.getUTCSeconds());
    document.getElementById('crono').innerHTML = result;

    // Indicamos que se ejecute esta función nuevamente dentro de 1 segundo
    timeout = setTimeout("funcionando()", 1000);
  }

  /* Funcion que pone un 0 delante de un valor si es necesario */
  function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : +Time;
  }

  window.onload = function() {
    if (localStorage.getItem("inicio") != null) {
      // Si al iniciar el navegador, la variable inicio que se guarda
      // en la base de datos del navegador tiene valor, cargamos el valor
      // y iniciamos el proceso.
      inicio = localStorage.getItem("inicio");
      document.getElementById("boton").value = "Detener";
      funcionando();
    }
  }
</script>

<style>
  .crono_wrapper {
    text-align: center;
    width: 200px;
  }

  .crono_wrapper {
    background-color: #eee;
    border: 0;
  }
</style>
</head>

<body>

  <div class="crono_wrapper">
    <h2 id='crono'>00:00:00</h2>
    <input id="boton" onclick="empezarDetener(this);">
  </div>