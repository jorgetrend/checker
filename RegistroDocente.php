<!doctype html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <title>Registrar Usuario</title>
</head>

<body>

  <nav class="navbar navbar-dark bg-success">
    <h3>Checker <span class="badge badge-secondary">Control</span></h3>
    <div class="container">
      <a href="index.php" class="navbar-brand"> Inicio</a>
      <a href="loginDocente.php" class="navbar-brand" style="color: yellowgreen"> Docente</a>
      <a href="loginEstudiante.php" class="navbar-brand"> Estudiante</a>
      <a href="informacion.php" class="navbar-brand"> Acerca de nosotros</a>
    </div>
  </nav>
  <div class="Contenedor">
    <form action="registrarDocente.php" class="formulario" method="post">
      <h1 class="titulo">Registrar Docente</h1>
      <div name="input" style="margin-top: 20px;">
        <input type="text" class="formulario__input" name="nombre_docente" required="true">
        <label type="text" class="formulario__label">Nombre</label>

        <input type="text" class="formulario__input" name="matricula_docente" required="true">
        <label type="text" class="formulario__label">Matricula</label>

        <input type="email" class="formulario__input" name="correo_docente" required="true">
        <label type="text" class="formulario__label">Correo</label>

        <input type="password" class="formulario__input" name="contrasena_docente" required="true" minlength="8">
        <label type="text" class="formulario__label">Contraseña</label>
      </div>
      <button type="submit" class="btn btn-primary btn-lg btn-block mb-3" name="submit">Registrar</button>
      <center>
        <h6 class="s1">¿Ya tienes una cuenta? </h6>
        <br>
        <a class="s2" href="loginDocente.php">Inicia sesión aquí</a>
      </center>
    </form>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script type="text/javascript" src="js/formulario.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <?php
  include("includes/footerDocente.php");
  ?>
</body>

</html>
