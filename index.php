<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Checker</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style type="text/css" >
        *{
            margin: 0;
            padding: 0;
        }
        #img1{
            padding: 80px 505px;
            display: block;

        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-dark bg-success">
       <h3>Checker <span class="badge badge-secondary">Control</span></h3>
      <div class="container">
          <a href="index.php" class="navbar-brand" style="color: yellowgreen" > Inicio</a>
          <a href="loginDocente.php" class="navbar-brand" > Docente</a>
          <a href="loginEstudiante.php" class="navbar-brand" > Estudiante</a>
          <a href="informacion.php" class="navbar-brand" > Acerca de nosotros</a>
      </div>
    </nav>
    <div id="Imagen">
        <div class="Img" id="img1">
            <img src="img/checkerW.PNG" class="d-block w-90" alt="First iamgen" width="370">
        </div>
    </div>
    <div class="Frase" style="text-align: center">
        <em><p>"El compromiso individual con un esfuerzo colectivo es lo que hace que un equipo,
          una empresa o una sociedad funcionen"</p></em> <h6>Vince Lombardi</h6>
    </div>
    <?php
      include("includes/footerDocente.php");
    ?>
  </body>
</html>
