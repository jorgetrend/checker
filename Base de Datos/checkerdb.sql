-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-07-2020 a las 02:33:59
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `checkerdb`
--
DROP DATABASE IF EXISTS checkerdb;
CREATE DATABASE checkerdb;
USE checkerdb;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `id_actividad` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `id_estudiante` int(11) DEFAULT NULL,
  `id_ubicacion` int(11) DEFAULT NULL,
  `id_prioridad` int(11) NOT NULL,
  `id_estado_act` int(11) NOT NULL DEFAULT '1',
  `descripcion_act` varchar(500) NOT NULL,
  `fecha_act` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `observacion_act` varchar(500) DEFAULT NULL,
  `observacion_doc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`id_actividad`, `id_docente`, `id_estudiante`, `id_ubicacion`, `id_prioridad`, `id_estado_act`, `descripcion_act`, `fecha_act`, `observacion_act`, `observacion_doc`) VALUES
(71, 89, 34, 30, 3, 2, 'Fumar hierba', '2020-07-23 02:07:06', 'Bien rikolina', ''),
(72, 89, 34, 30, 3, 0, 'Quitarse la ropa', '2020-07-22 19:09:02', 'Dale sin miedo', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `id_docente` int(11) NOT NULL,
  `nombre_doc` varchar(100) NOT NULL,
  `matricula_doc` varchar(9) NOT NULL,
  `correo_doc` varchar(150) NOT NULL,
  `contrasena_doc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`id_docente`, `nombre_doc`, `matricula_doc`, `correo_doc`, `contrasena_doc`) VALUES
(87, 'Jorge Luis Marcelo Hipólito', '26', 'jorgetrend4@gmail.com', '$2y$10$0ngNPDgCh9DF6MW3X8.fle86Hlu5CeFrje5saEJ2ku0wNfP0bdvsG'),
(88, 'Karelia', '07', 'karelia@gmail.com', '$2y$10$YQsvzSpwssEJRoROimoELepXWfs78RuIGeRB6UPCZpZ1umyja2ABq'),
(89, 'Manolo', 'S10', 'Manolo@uv.mx', '$2y$10$V4QD16pneaRs2gIUjShLZepWVzrH3Kjl6hzxmac009Smc34iKGx6O');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_actividad`
--

CREATE TABLE `estado_actividad` (
  `id_estado_act` int(11) NOT NULL,
  `descripcion_estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_actividad`
--

INSERT INTO `estado_actividad` (`id_estado_act`, `descripcion_estado`) VALUES
(0, 'Pendiente'),
(1, 'En curso'),
(2, 'En pausa'),
(3, 'Esperando evaluación'),
(4, 'Concluida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_estudiante`
--

CREATE TABLE `estado_estudiante` (
  `id_estado` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_estudiante`
--

INSERT INTO `estado_estudiante` (`id_estado`, `Descripcion`) VALUES
(1, 'Disponible'),
(2, 'Ocupado'),
(3, 'Desconectado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_servicio`
--

CREATE TABLE `estado_servicio` (
  `id_estado_ser` int(11) NOT NULL,
  `descripcion_estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_servicio`
--

INSERT INTO `estado_servicio` (`id_estado_ser`, `descripcion_estado`) VALUES
(1, 'En proceso'),
(2, 'Terminado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `id_estudiante` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `nombre_est` varchar(100) NOT NULL,
  `matricula_est` varchar(9) NOT NULL,
  `licenciatura_est` varchar(100) NOT NULL,
  `facultad_est` varchar(100) NOT NULL,
  `correo_est` varchar(150) NOT NULL,
  `contrasena_est` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`id_estudiante`, `id_docente`, `id_estado`, `nombre_est`, `matricula_est`, `licenciatura_est`, `facultad_est`, `correo_est`, `contrasena_est`) VALUES
(29, 87, 1, 'Maria Hipolito Lopez', '23', 'Ingenieria de software', 'Contaduria y Administracion', 'maria@hotmail.com', '$2y$10$ryPIdkm4Ctd7Fha7okab3.tk4kk0mM3OaPBiGiCg.8QsatJpgENRi'),
(30, 87, 3, 'Itzel Marcelo', '25', 'Administracion', 'Contaduria y Administracion', 'itzel@hotmail.com', '$2y$10$aygdNYIZckvkoYryNxM/k.ZXQlJRlRo0YmHTUj8.Canlv3cbG2PvG'),
(31, 87, 3, 'Santiago', '24', 'Ingenieria de software', 'Contaduria y Administracion', 'santiago@gmail.com', '$2y$10$40047HRX0cedulJyxwIJ2OujR9rDD40.SwVBIgeBFn0WjXgPX58dS'),
(32, 87, 3, 'Daniel Dominguez', '34', 'Ingenieria de software', 'Contaduria y Administracion', 'daniel@hotmail.com', '$2y$10$z5awie7ATrIDri832dbcteV25Ho0caHkQuBL3l7zElSKkqUC1nrye'),
(33, 88, 3, 'Sergio Torres', '798', 'Ingenieria de software', 'Contaduria y Administracion', 'sergio@gmail.com.mx', '$2y$10$xXpGdmOmWuAcvrav0DqP1e4P6.JICcrI2hCIQoAJDd3bzGbt05k6S'),
(34, 89, 1, 'Sr pool', 'S22', 'Ingenieria de software', 'FCAA', 'DrPool@uv.mx', '$2y$10$02lMiKTBfUPMb9aAPf7v6OyfE70PW9o.vHhKFl8hvSa2qoAKPbr7C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id_permiso` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `descripcion_per` varchar(300) NOT NULL,
  `fecha_per` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id_permiso`, `id_estudiante`, `id_docente`, `descripcion_per`, `fecha_per`) VALUES
(25, 33, 88, 'Falta por viajar ', '2020-07-22 22:31:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prioridad_act`
--

CREATE TABLE `prioridad_act` (
  `id_prioridad` int(11) NOT NULL,
  `descripcion_pri` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `prioridad_act`
--

INSERT INTO `prioridad_act` (`id_prioridad`, `descripcion_pri`) VALUES
(1, 'Baja'),
(2, 'Alta'),
(3, 'Muy alta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguimiento_docente`
--

CREATE TABLE `seguimiento_docente` (
  `id_seguimiento_doc` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguimiento_docente`
--

INSERT INTO `seguimiento_docente` (`id_seguimiento_doc`, `id_docente`, `descripcion`, `fecha_creacion`) VALUES
(77, 89, 'Elimino permiso con Id 29', '2020-07-23 01:53:58'),
(78, 89, 'Registro ubicacion Trabajo', '2020-07-23 01:57:09'),
(79, 89, 'Registro ubicacion Mi casa', '2020-07-23 01:58:26'),
(80, 89, 'Elimino ubicacion Mi casa', '2020-07-23 01:59:49'),
(81, 89, 'Actualizacion del perfil  Manolo', '2020-07-23 02:02:39'),
(82, 89, 'Ingreso a cuenta', '2020-07-22 19:08:32'),
(83, 89, 'Creacion de actividad Quitarse la ropa', '2020-07-22 19:09:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguimiento_estudiante`
--

CREATE TABLE `seguimiento_estudiante` (
  `id_seguimiento` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descripcion` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguimiento_estudiante`
--

INSERT INTO `seguimiento_estudiante` (`id_seguimiento`, `id_estudiante`, `fecha_creacion`, `descripcion`) VALUES
(1, 34, '2020-07-23 02:07:06', 'Agrego actividad con id Fumar hierba'),
(2, 34, '2020-07-23 02:10:32', 'Tomo actividad Quitarse la ropa'),
(4, 34, '2020-07-23 02:19:41', 'Concluyo actividad Quitarse la ropa'),
(5, 34, '2020-07-23 02:21:55', 'Comenzo actividad Fumar hierba'),
(6, 34, '2020-07-23 02:22:58', 'Pauso actividad Fumar hierba'),
(7, 34, '2020-07-23 02:25:53', 'actualizo perfil Sr pool');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_social`
--

CREATE TABLE `servicio_social` (
  `id_estudiante` int(11) NOT NULL,
  `id_estado_ser` int(11) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_fin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `horas_restantes` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `id_sesion` int(11) NOT NULL,
  `matricula_est` varchar(9) NOT NULL,
  `fecha_sesion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tema`
--

CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL,
  `id_docente` int(11) DEFAULT NULL,
  `id_estudiante` int(11) DEFAULT NULL,
  `color_header` varchar(50) NOT NULL DEFAULT 'bg-danger'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tema`
--

INSERT INTO `tema` (`id_tema`, `id_docente`, `id_estudiante`, `color_header`) VALUES
(119, 87, NULL, 'bg-primary'),
(120, NULL, 29, 'bg-danger'),
(121, NULL, 30, 'bg-primary'),
(122, NULL, 31, 'bg-primary'),
(123, NULL, 32, 'bg-primary'),
(124, 88, NULL, 'bg-primary'),
(125, NULL, 33, 'bg-primary'),
(126, 89, NULL, 'bg-primary'),
(127, NULL, 34, 'bg-primary');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempo_servicio`
--

CREATE TABLE `tiempo_servicio` (
  `id_tiempo_servicio` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `hora_inicio` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hora_fin` datetime DEFAULT NULL,
  `total_horas` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tiempo_servicio`
--

INSERT INTO `tiempo_servicio` (`id_tiempo_servicio`, `id_estudiante`, `hora_inicio`, `hora_fin`, `total_horas`) VALUES
(22, 29, '2020-07-22 15:38:39', '2020-07-22 15:38:56', '17'),
(23, 29, '2020-07-22 15:39:08', '2020-07-22 15:49:41', '633'),
(24, 29, '2020-07-22 15:50:53', NULL, NULL),
(25, 34, '2020-07-22 18:16:07', '2020-07-22 18:16:31', '24'),
(26, 34, '2020-07-22 18:16:44', '2020-07-22 18:17:10', '26'),
(27, 34, '2020-07-22 18:17:16', '2020-07-22 18:27:22', '606'),
(28, 34, '2020-07-22 18:28:59', '2020-07-22 19:02:58', '2039'),
(29, 34, '2020-07-22 19:03:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

CREATE TABLE `ubicacion` (
  `id_ubicacion` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `nombre_ubi` varchar(100) NOT NULL,
  `descripcion_ubi` varchar(150) DEFAULT 'Esta ubicación no cuenta con alguna descripción.',
  `imagen_ubi` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ubicacion`
--

INSERT INTO `ubicacion` (`id_ubicacion`, `id_docente`, `nombre_ubi`, `descripcion_ubi`, `imagen_ubi`) VALUES
(28, 87, 'Cueva', 'Cuevaa forever', 'ImagenesUsuarios/Ubicacion/fondo_pantalla.png'),
(30, 89, 'Cancha deportiva 1', 'Junto a la cooperativa', 'ImagenesUsuarios/Ubicacion/fondo.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`id_actividad`),
  ADD KEY `id_docente` (`id_docente`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_ubicacion` (`id_ubicacion`),
  ADD KEY `id_prioridad` (`id_prioridad`),
  ADD KEY `id_estado_act` (`id_estado_act`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`id_docente`);

--
-- Indices de la tabla `estado_actividad`
--
ALTER TABLE `estado_actividad`
  ADD PRIMARY KEY (`id_estado_act`);

--
-- Indices de la tabla `estado_estudiante`
--
ALTER TABLE `estado_estudiante`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `estado_servicio`
--
ALTER TABLE `estado_servicio`
  ADD PRIMARY KEY (`id_estado_ser`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`id_estudiante`),
  ADD KEY `id_estado` (`id_estado`),
  ADD KEY `id_docente` (`id_docente`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id_permiso`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_docente` (`id_docente`);

--
-- Indices de la tabla `prioridad_act`
--
ALTER TABLE `prioridad_act`
  ADD PRIMARY KEY (`id_prioridad`);

--
-- Indices de la tabla `seguimiento_docente`
--
ALTER TABLE `seguimiento_docente`
  ADD PRIMARY KEY (`id_seguimiento_doc`),
  ADD KEY `id_docente` (`id_docente`);

--
-- Indices de la tabla `seguimiento_estudiante`
--
ALTER TABLE `seguimiento_estudiante`
  ADD PRIMARY KEY (`id_seguimiento`),
  ADD KEY `id_estudiante` (`id_estudiante`);

--
-- Indices de la tabla `servicio_social`
--
ALTER TABLE `servicio_social`
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_estado_ser` (`id_estado_ser`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`id_sesion`);

--
-- Indices de la tabla `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_tema`),
  ADD KEY `id_docente` (`id_docente`),
  ADD KEY `id_estudiante` (`id_estudiante`);

--
-- Indices de la tabla `tiempo_servicio`
--
ALTER TABLE `tiempo_servicio`
  ADD PRIMARY KEY (`id_tiempo_servicio`),
  ADD KEY `id_estudiante` (`id_estudiante`);

--
-- Indices de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`id_ubicacion`),
  ADD KEY `id_docente` (`id_docente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `id_actividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `docente`
--
ALTER TABLE `docente`
  MODIFY `id_docente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `estado_actividad`
--
ALTER TABLE `estado_actividad`
  MODIFY `id_estado_act` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `estado_estudiante`
--
ALTER TABLE `estado_estudiante`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estado_servicio`
--
ALTER TABLE `estado_servicio`
  MODIFY `id_estado_ser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `id_estudiante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `prioridad_act`
--
ALTER TABLE `prioridad_act`
  MODIFY `id_prioridad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `seguimiento_docente`
--
ALTER TABLE `seguimiento_docente`
  MODIFY `id_seguimiento_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT de la tabla `seguimiento_estudiante`
--
ALTER TABLE `seguimiento_estudiante`
  MODIFY `id_seguimiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sesion`
--
ALTER TABLE `sesion`
  MODIFY `id_sesion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tema`
--
ALTER TABLE `tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT de la tabla `tiempo_servicio`
--
ALTER TABLE `tiempo_servicio`
  MODIFY `id_tiempo_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `id_ubicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `actividad_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `actividad_ibfk_2` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiante` (`id_estudiante`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `actividad_ibfk_3` FOREIGN KEY (`id_ubicacion`) REFERENCES `ubicacion` (`id_ubicacion`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `actividad_ibfk_4` FOREIGN KEY (`id_prioridad`) REFERENCES `prioridad_act` (`id_prioridad`),
  ADD CONSTRAINT `actividad_ibfk_5` FOREIGN KEY (`id_estado_act`) REFERENCES `estado_actividad` (`id_estado_act`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD CONSTRAINT `estudiante_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `estado_estudiante` (`id_estado`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `estudiante_ibfk_3` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD CONSTRAINT `permiso_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiante` (`id_estudiante`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permiso_ibfk_2` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`);

--
-- Filtros para la tabla `seguimiento_docente`
--
ALTER TABLE `seguimiento_docente`
  ADD CONSTRAINT `seguimiento_docente_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `seguimiento_estudiante`
--
ALTER TABLE `seguimiento_estudiante`
  ADD CONSTRAINT `seguimiento_estudiante_ibfk_2` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiante` (`id_estudiante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicio_social`
--
ALTER TABLE `servicio_social`
  ADD CONSTRAINT `servicio_social_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiante` (`id_estudiante`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_social_ibfk_2` FOREIGN KEY (`id_estado_ser`) REFERENCES `estado_servicio` (`id_estado_ser`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `tema`
--
ALTER TABLE `tema`
  ADD CONSTRAINT `tema_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tema_ibfk_2` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiante` (`id_estudiante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tiempo_servicio`
--
ALTER TABLE `tiempo_servicio`
  ADD CONSTRAINT `tiempo_servicio_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiante` (`id_estudiante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD CONSTRAINT `ubicacion_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
