<!doctype html>
<html lang="es">
<?php session_start();
if (isset($_SESSION['matricula_docente'])) { // Si existe una sesión abierta, que rediriga a InicioDocente.
  header("location: InicioDocente.php");
}
?>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- CSS personalizable -->
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <!-- Titulo -->
  <title>Login</title>
</head>

<body>
  <nav class="navbar navbar-dark bg-success">
    <h3>Checker <span class="badge badge-secondary">Control</span></h3>
    <div class="container">
      <a href="index.php" class="navbar-brand"> Inicio</a>
      <a href="loginDocente.php" class="navbar-brand" style="color: yellowgreen"> Docente</a>
      <a href="loginEstudiante.php" class="navbar-brand"> Estudiante</a>
      <a href="informacion.php" class="navbar-brand"> Acerca de nosotros</a>
    </div>
  </nav>
  <!-- Declaración de la hora -->
  <!-- formulario-->
  <form action="validarDocente.php" class="formulario" method="POST">
    <h1 class="titulo">Iniciar sesión</h1>
    <div class="inputs" style="margin-top: 20px;">
      <input type="text" class="formulario__input" name="matricula_docente" required="true">
      <label type="text" class="formulario__label">Matrícula</label>

      <input type="password" class="formulario__input" name="contrasena_docente" required="true" minlength="8">
      <label type="text" class="formulario__label">Contraseña</label>
    </div>
    <button type="submit" class="btn btn-primary btn-lg btn-block mb-3" name="submit">Ingresar</button>

    <center>
      <h6 class="s1">¿Aún no tienes una cuenta? </h6>
      <br>
      <a class="s2" href="RegistroDocente.php">Regístrate aquí</a>
      <center>
        <button style="margin-top: 30px" type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#restablecerPass" data-whatever="@mdo">Restablecer contraseña
        </button>
      </center>
    </center>
  </form>
  <br>
  <div class="container mt-2 pt-2">
    <div class="modal fade" id="restablecerPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-success text-white">
            <h5 class="modal-title" id="exampleModalLabel">Restablecer Contraseña</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <form action="RestablecerPass.php" method="post">
                <p>Ingresa tu correo electrónico.</p>
                <input type="text" class="form-control" id="correo" name="correo_pass" placeholder="mail@example.com" required>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-danger">Enviar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include("includes/footerDocente.php"); ?>
</body>

</html>