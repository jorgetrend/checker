<?php
include("BD.php");
include("includes/headerEstudiante.php");
if (isset($_SESSION['matricula_estudiante'])) {
?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="Inicioestudiante.php">Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mi Perfil</li>
      </ol>
    </nav>
    <div class="container">
      <div class="textoPrincipal" style="text-align: center; margin-top:10px;">
        <h2>Mi Perfil</h2>
        <hr>
      </div>
    </div>
    <br>
    <div class="card" style=" box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        margin: auto;
        text-align: center;

        font-family: arial;">
      <center>
        <img src="imges/user.png" alt="John" style="width:25%; margin-top:20px; margin-bottom: 20px;">
      </center>
      <?php
      $matricula = $_SESSION['matricula_estudiante'];
      $consulta = "Select * from estudiante WHERE matricula_est = '$matricula'";
      $query = mysqli_query($conexion_BD, $consulta);
      $arrayestudiante = mysqli_fetch_array($query);
      ?>
      <h2><?php echo $arrayestudiante['nombre_est']; ?></h2>
      <hr>
      <p class="text">Matricula: <?php echo $arrayestudiante['matricula_est']; ?></p>
      <p class="text">Licenciatura: <?php echo $arrayestudiante['licenciatura_est']; ?></p>
      <p class="text">Facultad: <?php echo $arrayestudiante['facultad_est']; ?></p>

      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModa15" data-whatever="@mdo">Editar informacion
      </button>

      <div class="modal fade" id="exampleModa15" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h5 class="modal-title" id="exampleModalLabel">Ingresa los datos a actualizar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="ActualizarEstudiante.php" method="POST">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nombre:</label>
                  <input type="text" class="form-control" id="recipient-name" name="nombre" placeholder="Ej. Docente Maravilla" value='<?php echo $arrayestudiante['nombre_est']; ?>'required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Matricula:</label>
                  <input type="text" class="form-control" id="message-text" name="matricula" placeholder="Ej. S17016281" value='<?php echo $arrayestudiante['matricula_est']; ?>'required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Licenciatura:</label>
                  <input type="text" class="form-control" id="message-text" name="licenciatura" placeholder="Ej. Ing. Software" value='<?php echo $arrayestudiante['licenciatura_est']; ?>' required="true">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Facultad:</label>
                  <input type="text" class="form-control" id="message-text" name="facultad" placeholder="Ej. FCA" required="true" value='<?php echo $arrayestudiante['facultad_est']; ?>'>
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Contraseña del estudiante:</label>
                  <input type="password" class="form-control" name="contrasena" placeholder="Ej. ************" required="true" minlength="8">
                </div>


                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <input type="submit" value="Actualizar" class="btn btn-primary" name="submit">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>



  </body>
  <?php include("includes/footer.php"); ?>
<?php } else {
  header("location: index.php");
} ?>