<?php include("BD.php"); ?>
<?php include("includes/headerEstudiante.php"); ?>
<?php
if (isset($_SESSION['matricula_estudiante'])) {
?>

  <body>

    <div class="container">
      <div class="textoPrincipal" style="text-align: center; margin-top:10px;">
        <h2>Temas</h2>
        <hr>
        <p>Selecciona la opción que deseas aplicar.</p>
      </div>
    </div>
    <br>

    <form action="ActualizarTemaEst.php" method="post">
      <div class="cabecera" style="text-align: center">
        <!--  -->
        <input type="radio" id="dark" name="navbar" value="bg-dark" style="margin-top:20px" checked>
        <label for="dark">Tema oscuro</label>

        <nav class="navbar navbar-dark bg-dark mb-3 " style="width: 75%; margin-left:10%;">
          <img src="img/logo.png" alt="Logo Checker" style="width: 40px; height: 40px;" />

          <a href="#" class="navbar-brand"> Actividades</a>
          <a href="#" class="navbar-brand"> Reportes</a>
          <div class="dropdown">
            <button style="color:white" class="btn btn-succes dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $nombre = $_SESSION['Estudiante']['nombre_est'];
              echo $nombre; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="PerfilDocente.php">Perfíl</a>
              <a class="dropdown-item" href="temaEstudiante.php">Temas</a>

              <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
            </div>
          </div>
        </nav>

        <input type="radio" id="primary" name="navbar" value="bg-primary">
        <label for="primary">Tema primario</label>

        <nav class="navbar navbar-dark bg-primary mb-3" style="width: 75%; margin-left:10%;">
          <img src="img/logo.png" alt="Logo Checker" style="width: 40px; height: 40px;" />

          <a href="#" class="navbar-brand"> Actividades</a>
          <a href="#" class="navbar-brand"> Reportes</a>
          <div class="dropdown">
            <button style="color:white" class="btn btn-succes dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $nombre = $_SESSION['Estudiante']['nombre_est'];
              echo $nombre; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="PerfilDocente.php">Perfíl</a>
              <a class="dropdown-item" href="temaEstudiante.php">Temas</a>

              <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
            </div>
          </div>
        </nav>

        <input type="radio" id="secondary" name="navbar" value="bg-secondary">
        <label for="secondary">Tema secundario</label>

        <nav class="navbar navbar-dark bg-secondary mb-3" style="width: 75%; margin-left:10%;">
          <img src="img/logo.png" alt="Logo Checker" style="width: 40px; height: 40px;" />

          <a href="#" class="navbar-brand"> Actividades</a>
          <a href="#" class="navbar-brand"> Reportes</a>
          <div class="dropdown">
            <button style="color:white" class="btn btn-succes dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $nombre = $_SESSION['Estudiante']['nombre_est'];
              echo $nombre; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="PerfilDocente.php">Perfíl</a>
              <a class="dropdown-item" href="temaEstudiante.php">Temas</a>

              <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
            </div>
          </div>
        </nav>

        <input type="radio" id="danger" name="navbar" value="bg-danger">
        <label for="danger" style="margin-left: 20px">Tema danger</label>

        <nav class="navbar navbar-dark bg-danger mb-3" style="width: 75%; margin-left:10%;">
          <img src="img/logo.png" alt="Logo Checker" style="width: 40px; height: 40px;" />

          <a href="#" class="navbar-brand"> Actividades</a>
          <a href="#" class="navbar-brand"> Reportes</a>
          <div class="dropdown">
            <button style="color:white" class="btn btn-succes dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $nombre = $_SESSION['Estudiante']['nombre_est'];
              echo $nombre; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="PerfilDocente.php">Perfíl</a>
              <a class="dropdown-item" href="temaEstudiante.php">Temas</a>

              <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
            </div>
          </div>
        </nav>

        <input type="radio" id="warning" name="navbar" value="bg-warning">
        <label for="warning">Tema warning</label>

        <nav class="navbar navbar-light bg-warning mb-3" style="width: 75%; margin-left:10%;">
          <img src="img/logo.png" alt="Logo Checker" style="width: 40px; height: 40px;" />

          <a href="#" class="navbar-brand"> Actividades</a>
          <a href="#" class="navbar-brand"> Reportes</a>
          <div class="dropdown">
            <button style="color:white" class="btn btn-succes dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $nombre = $_SESSION['Estudiante']['nombre_est'];
              echo $nombre; ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="PerfilDocente.php">Perfíl</a>
              <a class="dropdown-item" href="temaEstudiante.php">Temas</a>

              <a class="dropdown-item" href="cerrarSesion.php">Cerrar sesión</a>
            </div>
          </div>
        </nav>
      </div>


      <div class="botonAceptar" style="margin-left: 75%; margin-top:40px;">
        <button type="submit" class="btn btn-success mb-3">Aplicar Cambios</button>
      </div>
    </form>

  </body>
<?php include("includes/footer.php");
} else {
  header("location: index.php");
} ?>
