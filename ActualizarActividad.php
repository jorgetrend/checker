<?php
include("BD.php");

$id_actividad = $_POST['id'];
$descripcion_actividad    = $_POST['descripcion'];

$nombre_ubicacion  = $_POST['ubicacion'];
$consultaUbicacion = "SELECT id_ubicacion from ubicacion where nombre_ubi = '$nombre_ubicacion'";
$queryUbicacion = mysqli_query($conexion_BD, $consultaUbicacion);
$arrayUbicacion = mysqli_fetch_array($queryUbicacion);
$id_ubicacion = $arrayUbicacion['id_ubicacion'];

$prioridad    = $_POST['prioridad'];
switch ($prioridad) {
    case "Baja":
        $prioridad = 1;
        break;
    case "Alta":
        $prioridad = 2;
        break;
    case "Muy Alta":
        $prioridad = 3;
        break;
    default:
        break;
}

session_start();
$id_docente = $_SESSION['docente']['id_docente'];

if (empty($_POST['descripcion'] && empty($_POST['ubicacion'] && empty($_POST['prioridad'])))) {
    echo "Datos no válidos:(";
} else {

    $sql = "UPDATE actividad SET descripcion_act = '$descripcion_actividad', id_ubicacion = '$id_ubicacion', id_prioridad = '$prioridad' where  id_actividad = '$id_actividad'";

    $ejecutar = mysqli_query($conexion_BD, $sql);
    if (!$ejecutar) {
        echo "Error al actualizar";
    } else {
        $log = "modifico de actividad" . " " . $descripcion_actividad . "Con Id " . " " . $id_actividad;
        date_default_timezone_set('America/Mexico_City');
        $fecha_actividad = date("Y-m-d H:i:s");
        $seguimiento = "INSERT INTO seguimiento_docente VALUES(null, '$id_docente', '$log', '$fecha_actividad')";
        mysqli_query($conexion_BD, $seguimiento);
        echo '<script>
    			alert("Actividad registrada correctamente");
    			</script>';
        header("location: InicioActividades.php");
        die();
    }
}
