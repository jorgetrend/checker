<?php include("includes/header.php"); ?>
<?php include("BD.php"); ?>

<?php
if (isset($_SESSION['matricula_docente'])) {
?>

	<body>
		<!--Se agrega el camino de migajas en la parte superior-->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="InicioDocente.php">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ubicaciones</li>
			</ol>
		</nav>
		<div class="container">
			<div class="textoPrincipal" style="text-align: center; margin-top:10px;">
				<h2>Ubicaciones</h2>
				<hr>
				<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
					Agregar Ubicación
				</button>
				<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">
					Editar Ubicación
				</button>
				<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal3" data-whatever="@mdo">
					Eliminar Ubicación
				</button>
			</div>
		</div>


		<div class="container mt-5 pt-10 ">
			<div class="row">
				<?php
				$id_docente = $_SESSION['docente']['id_docente'];
				$query = "SELECT * FROM ubicacion where id_docente = '$id_docente'";
				$resultadoUbicaciones = mysqli_query($conexion_BD, $query);
				$total = mysqli_num_rows($resultadoUbicaciones);
				if ($total == 0) {
				?>
					<div style="margin-left:auto; margin-right:auto;">
						<img style="height: 150px;" src="ImagenesUsuarios/Figuras/searchingLocation.png" alt="vector Buscar">
						<h6 style="margin-bottom: 20px; margin-top:20px;">No hay ubicaciones registradas :(</h6>
						<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" style="margin-left: 50px;">
							Agregar Ubicación
						</button>
					</div>
				<?php
				}
				while ($card = mysqli_fetch_array($resultadoUbicaciones)) {
				?>
					<div class="col-sm-4" style="margin-top: 2rem; min-width:300px">
						<?php
						$imagen = $card['imagen_ubi']; ?>
						<div class="card" style="width: 18rem;">
							<img class="card-img-top" <?php echo "<img src='$imagen' height='150px'"  ?> alt="Imagen de la ubicación.">
							<div class="card-body">
								<h5 class="card-title"><?php echo $card['nombre_ubi']; ?></h5>
								<hr>
								<p class="card-text"><?php echo $card['descripcion_ubi']; ?></p>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<!--Fin Ubicaciones guardadas-->


		<!--Agregar ubicación-->
		<div class="container mt-5 pt-5">

			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">

					<div class="modal-content">
						<div class="modal-header bg-primary text-white">
							<h5 class="modal-title" id="exampleModalLabel">Registrar Ubicación</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<form action="registrarUbicacion.php" method="POST" enctype="multipart/form-data">
								<p>Ingresa los datos requeridos para registrar una nueva ubicación.</p>
								<hr>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Nombre de la ubicación:</label>
									<input type="text" class="form-control" id="recipient-name" name="ubicacion" placeholder="Ej. La Cueva" required="true">
								</div>

								<div class="form-group">
									<label for="message-text" class="col-form-label">Descripcion de la ubicación:</label>
									<textarea class="form-control" id="message-text" name="descripcion" placeholder="Ej. El mejor lugar." required="true"></textarea>
								</div>

								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" name="image" required="true">
										<label class="custom-file-label" for="inputGroupFile04">Selecciona la imagen a cargar...</label>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									<input type="submit" value="Registrar ubicación" class="btn btn-primary" name="submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Fin Agregar ubicación-->

		<!--Editar ubicación-->
		<div class="container mt-5 pt-5">

			<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">

					<div class="modal-content">
						<div class="modal-header bg-warning text-white">
							<h5 class="modal-title" id="exampleModalLabel">Editar Ubicación</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<form action="editarUbicacion.php" method="POST" enctype="multipart/form-data">
								<p>Ingresa los datos requeridos para editar una ubicación.</p>
								<hr>
								<div class="form-group">
									<label for="message-text" class="col-form-label">Ubicación:</label>
									<?php
									$consulta = "SELECT * FROM ubicacion where  id_docente = '$id_docente'";
									$query = mysqli_query($conexion_BD, $consulta); ?>
									<select name="ubicacion_elegida" required="true">
										<?php while ($ubicaciones = mysqli_fetch_assoc($query)) { ?>
											<option> <?php echo $ubicaciones['nombre_ubi'] ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Nombre de la ubicación:</label>
									<input type="text" class="form-control" id="recipient-name" name="ubicacion" placeholder="Ej. La Cueva" required="true">
								</div>

								<div class="form-group">
									<label for="message-text" class="col-form-label">Descripcion de la ubicación:</label>
									<textarea class="form-control" id="message-text" name="descripcion" placeholder="Ej. Descripción de la cueva." required="true"></textarea>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									<input type="submit" value="Editar ubicación" class="btn btn-warning" name="submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Fin Editar ubicación-->

		<!--Eliminar ubicación-->
		<div class="container mt-5 pt-5">

			<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">

					<div class="modal-content">
						<div class="modal-header bg-danger text-white">
							<h5 class="modal-title" id="exampleModalLabel">Eliminar Ubicación</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<form action="eliminarUbicacion.php" method="POST" enctype="multipart/form-data">
								<p>Ingresa los datos requeridos para eliminar una ubicación.</p>
								<hr>
								<div class="form-group">
									<label for="message-text" class="col-form-label">Ubicación:</label>
									<?php
									$consulta = "SELECT * FROM ubicacion where  id_docente = '$id_docente'";
									$query = mysqli_query($conexion_BD, $consulta); ?>
									<select name="ubicacion_elegida" required="true">
										<?php while ($ubicaciones = mysqli_fetch_assoc($query)) { ?>

											<option> <?php echo $ubicaciones['nombre_ubi'] ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									<input type="submit" value="Eliminar ubicación" class="btn btn-danger" name="submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Fin Eliminar ubicación-->

		<?php include("includes/footerDocente.php"); ?>
	</body>

	<!--Fin Agregar ubicación-->
<?php } else {
	header("location: index.php");
} ?>