<?php include("includes/header.php");
include("BD.php"); ?>

<?php
if (isset($_SESSION['matricula_docente'])) {

?>

  <body>
    <!--Se agrega el camino de migajas en la parte superior-->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="InicioDocente.php">Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mi Perfil</li>
      </ol>
    </nav>
    <div class="container">
      <div class="textoPrincipal" style="text-align: center; margin-top:10px;">
        <h2>Mi Perfil</h2>
        <hr>
      </div>
    </div>
    <br>
    <div class="card" style=" box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        margin: auto;
        text-align: center;
        
        font-family: arial;">
      <center>
        <img src="imges/user.png" alt="John" style="width:25%; margin-top:20px; margin-bottom: 20px;">
      </center>
      <?php
      $matricula = $_SESSION['matricula_docente'];
      $consulta = "Select * from docente WHERE matricula_doc = '$matricula'";
      $query = mysqli_query($conexion_BD, $consulta);
      $arrayDocente = mysqli_fetch_array($query);
      ?>
      <h2><?php echo $arrayDocente['nombre_doc']; ?></h2>
      <hr>
      <p class="text">Matricula: <?php echo $arrayDocente['matricula_doc']; ?></p>
      <p class="text">Correo: <?php echo $arrayDocente['correo_doc']; ?></p>
      <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Editar información
      </button>

    </div>



    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">

        <div class="modal-content">
          <div class="modal-header bg-success" style="color:white">
            <h5 class="modal-title" id="exampleModalLabel">Editar Información</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
            </button>
          </div>



          <div class="modal-body">
            <p>Ingresa la información para actualizar tu perfil.</p>
            <hr>
            <form action="ActualizarInformacionD.php" method="POST">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nombre:</label>
                <input type="text" class="form-control" id="recipient-name" name="nombre" placeholder="Ej. Docente Maravilla" value='<?php echo $arrayDocente['nombre_doc']; ?>' required="true">
              </div>

              <div class="form-group">
                <label for="message-text" class="col-form-label">Matricula:</label>
                <input type="text" class="form-control" id="message-text" name="matricula" placeholder="Ej. S17016281" value='<?php echo $arrayDocente['matricula_doc']; ?>' required="true"></input>
              </div>

              <div class="form-group">
                <label for="message-text" class="col-form-label">Correo Electrónico:</label>
                <input type="email" class="form-control" id="message-text" name="correo" placeholder="Ej. correo@correo.com" value='<?php echo $arrayDocente['correo_doc']; ?>' required="true"></input>
              </div>

              <div class="form-group">
                <label for="message-text" class="col-form-label">Contraseña del Docente:</label>
                <input type="password" class="form-control" name="contrasena" placeholder="Ej. ************" required="true" minlength="8"></input>
              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <input type="submit" value="Actualizar" class="btn btn-success" name="submit">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>


    <?php include("includes/footerDocente.php"); ?>
  </body>
<?php } else {
  header("location: index.php");
} ?>